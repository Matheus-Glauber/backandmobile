package br.com.danielabella.exemplo1;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "atividade", path = "atividade")
public interface AtividadeRepository extends PagingAndSortingRepository<Atividade, Integer>{

}
