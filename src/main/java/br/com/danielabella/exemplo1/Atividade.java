package br.com.danielabella.exemplo1;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor
public class Atividade {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String nomeAtividade;
	
	private String descricaoAtividade;

}
